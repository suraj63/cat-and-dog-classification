from train import Train
from keras import backend as K

class Predict:

    def predict(self,resize_img,train_model):
        model = train_model

        pred_class = model.predict_classes(resize_img)
        prediction_prob = model.predict(resize_img)
        print("Prediction:::::",prediction_prob)
        K.clear_session()
        if (pred_class == 0 and prediction_prob >= 0.2):
            result = "cat CAT"
        elif (pred_class == 1 and prediction_prob >= 0.2):
            result = "DOG"
        else:
            result = "Object could not be classified"

        # print("result:::", result)
        #
        # res = (f"Probability_that_image_is_a_{result}_is:_{prediction_prob}")
        # print("result:::", res)

        return result
