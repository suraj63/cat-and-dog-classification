from flask import Flask,jsonify,request,render_template
import base64
from io import BytesIO
import io
from PIL import Image
from keras.preprocessing import image as im
import cv2
# from keras.models import load_model
import numpy as np
from train import Train
from predict import Predict
import tensorflow as tf
trai = Train()
predic = Predict()
global graph
graph = tf.get_default_graph()

app = Flask(__name__)


@app.route('/',methods=['GET','POST'])
def main():
    return render_template('index1.html')

@app.route('/upload',methods=['GET','POST'])
def upload():
    img = request.form['data']


    img = img[img.find(',') + 1:]
    image_io = io.BytesIO()
    image_io.write(base64.b64decode(img))
    imag = Image.open(image_io)

    user_img = imag
    user_img = im.img_to_array(user_img)
    resize_img = cv2.resize(user_img, (150, 150))
    #
    resize_img = resize_img / 255
    resize_img = np.expand_dims(resize_img, axis=0)

    train_model = trai.train()

    # with graph.as_default():
    pred = predic.predict(resize_img,train_model)

    return render_template('index1.html',data=pred)







if __name__=='__main__':
    app.run(debug=True,port='5000')